package TemporaryPassword;
/*
 * 加密：
1.输入的第一个参数改为时间：time[] = {2018,12,12,12,12,12}数组
1.5 得到time的元时间：time0[]={2018,1,1,0,0,0};
2.计算分钟值：minute = time_to_mim(time) - time_to_mim(time0)
3.将minute转为6位数组 minute_arr[6]={}（不足6位高位补0）;传入原来的算法中



解密
1.读取锁内的时间：time_lock[]={2018,12,12,12,12,12};
1.5 得到time的元时间：time0[]={2018,1,1,0,0,0};
2.计算分钟值：minute = time_to_mim(time) - time_to_mim(time0)
3.将minute转为6位数组 minute_arr[6]={}（不足6位高位补0）;作为原算法的real_time
 */
public class decryptPwdV2 extends decryptPwd{
	/*
	 * 校验数据
	 * 输入：final int [] realTime；锁内的时间的年月日时分秒,6位数组
	 * 返回值
	 * -1：时间不合法
	 * -2：数据校验错误
	 * -3：密码已经过期
	 * -4：密码正确时间合法，但是超出2小时范围
	 * 1：密码正确
	 */
	@Override
	public int checkData(final int [] keyInputPwd,final int [] lockPwd,final int [] realTime){
		//1.5
		int [] time0 = new int[6];
		if(tool.isTimeOverYear(realTime)){
			time0[0]=realTime[0] - 1;
			System.out.println("解密密码：该密码跨年----");
		}else{
			time0[0]=realTime[0];
		}
		time0[1]=1;
		time0[2]=1;
		time0[3]=0;
		time0[4]=0;
		time0[5]=0;
		//2
		int minutes = tool.time_to_minute(realTime) - tool.time_to_minute(time0);
		//3
		int [] time_minute = tool.intToArr(minutes,6);
		//4
		return super.checkDataV2(keyInputPwd,lockPwd,time_minute);
	}
	
	
	
	
	
	
	

}
