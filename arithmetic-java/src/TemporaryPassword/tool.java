package TemporaryPassword;

public class tool {
	
	public static void pArr(String head,int [] a,String d) {
		System.out.print(head);
		for(int i =0;i<a.length;i++) {
			System.out.print(a[i]+d);
		}
		System.out.println();
	}
	/*
	 * 两个数组数据的特殊加法
	 * 	2 3 1 9 2 5
	 * 	8 9 7 6 4 5
	 * +
	 * ----------------
	 * 	0 2 8 5 8 0
	 * 异常返回：-1：数据长度不合法；-2:数据内容不合法
	 */
	public static int [] arrSpicialNum(final int [] time,final int [] pwd) {
		int [] num = new int[6];
		if(time.length != 6 || pwd.length != 6) {
			num[0] = -1;
			return num;
		}
		for(int i=0;i<6;i++) {
			if(time[i]<0 || time[i]>9 || pwd[i]<0 || pwd[i] > 9) {
				num[0] = -2;
				return num;
			}
			num[i] = time[i] + pwd[i];
			num[i] = num[i]>=10?num[i]%10:num[i];
		}
		return num;
	}
	/*
	 * 两个数组数据的特殊减法
	 * 	0 2 8 5 8 0
	 * 	8 9 7 6 4 5
	 * -
	 * ----------------
	 *	2 3 1 9 2 5
	 * 异常返回：-1：数据长度不合法；-2:数据内容不合法
	 * P.S.上面arrSpicialNum的逆运算
	 */	
	public static int [] arrSpicialSubtraction(final int [] num,final int [] pwd) {
		int [] time = new int[6];
		if(num.length != 6 || pwd.length != 6) {
			time[0] = -1;
			return time;
		}
		for(int i=0;i<6;i++) {
			if(num[i]<0 || num[i]>9 || pwd[i]<0 || pwd[i] > 9) {
				time[0] = -2;
				return time;
			}
			time[i] = num[i] - pwd[i];
			time[i] = time[i]<0?(time[i]+10):time[i];
		}		
		return time;
	}
	/*
	 * 获取两个数据叠加起来数据的CRC对应的数组
	 */
	public  static int [] getArrHashNum(int [] time,int [] pwd) {
		int [] crcNumArr = new int [3];
		if(time.length != 6 || pwd.length != 6) {
			crcNumArr[0] = -1;
			return crcNumArr;
		}
		int [] num = new int[12];
		for(int i=0;i<6;i++) {
			num[i] = time[i];
			num[i+6] = pwd[i];
		}
		int crcNum = hashCode(num, 12);
		crcNumArr[0] = crcNum/100;;
		crcNumArr[1] = crcNum/10%10;
		crcNumArr[2] = crcNum%10;
		return crcNumArr;
	}
	/*
	 * 混淆一个9位的数组
	 */
	final static int [] index = {1,3,5,7,0,2,4,6,8};//keep same as C
	public static int [] mixArrEn(int [] arr) {
		int [] out = new int[9];
		if(arr.length != 9) {
			out[0] = -1;
			return out;
		}
		for(int i=0;i<9;i++) {
			out[i] = arr[index[i]];
		}
		return out;
	}
	/*
	 * 解密混淆后的一个9位的数组
	 */
	public static int [] mixArrDe(int [] arr) {
		int [] out = new int[9];
		if(arr.length != 9) {
			out[0] = -1;
			return out;
		}
		for(int i=0;i<9;i++) {
			out[index[i]] = arr[i];
		}
		return out;
	}
	/*
	 * 获取数据的自定义hashCode
	 * 返回：0--999
	 */
	public static int hashCode(int [] Source, int len){
		final int []  CRC8Table = {
			    0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
			    0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
			    0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e,
			    0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
			    0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0,
			    0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
			    0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d,
			    0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
			    0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5,
			    0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
			    0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58,
			    0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
			    0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6,
			    0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
			    0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b,
			    0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
			    0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f,
			    0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
			    0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,
			    0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
			    0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c,
			    0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
			    0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1,
			    0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
			    0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49,
			    0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
			    0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4,
			    0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
			    0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a,
			    0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
			    0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7,
			    0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35,
			};

		int sum=0,crc8=0;
		int i=0;
		for(i=0;i<len;i++){
			sum = sum ^ Source[i];
			if(sum<0)sum=0;
			if(sum>255)sum=255;
	        crc8  = CRC8Table[sum]; 
		}
	   return  crc8%1000;
	}
	//判断是否是闰年函数
	//月份   1  2  3  4  5  6  7  8  9  10 11 12
	//闰年   31 29 31 30 31 30 31 31 30 31 30 31
	//非闰年 31 28 31 30 31 30 31 31 30 31 30 31
	//输入:年份
	//输出:该年份是不是闰年.1,是.0,不是
	public static boolean Is_Leap_Year(int year)
	{			  
		if(year%4==0) //必须能被4整除
		{ 
			if(year%100==0) 
			{ 
				if(year%400==0)return true;//如果以00结尾,还要能被400整除 	   
				else return false;   
			}else return true;   
		}else return false;	
	}	 		
	/*
	*功能：把当前时间转化为分钟（从2018年开始算）
	*
	*输入：int [] time:17 1 12 12 12 00 支持到2099年 十进制
	*返回：转化后的分钟数 返回0表示数据非法
	*/
	public static int time_to_minute(final int [] time)
	{
		int syear;
		int smon,sday,hour,min,sec;
		final int [] mon_table={31,28,31,30,31,30,31,31,30,31,30,31};
		int t;
		int seccount=0;
		if(time[0]<2017)return 0;
		if(time[0]>2099)return 0;
		syear=time[0];smon=time[1];sday=time[2];
		hour=time[3];min=time[4];sec=time[5];
		for(t=2018;t<syear;t++)	//把所有年份的秒钟相加
		{
			if(Is_Leap_Year(t))
				seccount+=31622400;//闰年的秒钟数
			else
				seccount+=31536000;			  //平年的秒钟数
		}
		smon-=1;
		for(t=0;t<smon;t++)	   //把前面月份的秒钟数相加
		{
				seccount+=mon_table[t]*86400;//月份秒钟数相加
				if(Is_Leap_Year(syear)&&t==1)seccount+=86400;//闰年2月份增加一天的秒钟数
		}
		seccount+=(sday-1)*86400;//把前面日期的秒钟数相加
		seccount+=hour*3600;//小时秒钟数
		seccount+=min*60;	 //分钟秒钟数
		seccount+=sec;//最后的秒钟加上去
	  return seccount/60;	
	}
	
	public static int arrToInt(int [] arr) {
		int sum=0;
		for(int i=0;i<arr.length;i++) {
			sum+=arr[i]*Math.pow(10,arr.length-1-i);
		}
		return sum;
	}
	public static int [] intToArr(int src,int arr_len) {
		int start_index = arr_len - String.valueOf(src).length();
        int[] y = new int[arr_len];
        if(start_index < 0){
        	y[0]=-1;return y;
        }
        for(int i=0;i<start_index;i++){
        	y[i] = 0;
        }
        for(int i=start_index;i<arr_len;i++)
        { 
            y[i]= String.valueOf(src).toCharArray()[i-start_index]-48;
        }
        return y;
	}
	/*
	 * 判断时间是否合法  {2018,1,1,0,0,0};
	 */
	public static boolean isFullTimeLegal(final int [] time){
		if(time.length!=6)return false;
		for(int i = 0;i<time.length;i++){
			if(time[i]<0)return false;
		}
		if(time[0] >2099 || time[0] < 2018)return false;
		if(time[1] > 12)return false;
		if(time[2] > 31)return false;
		if(time[3] > 24)return false;
		if(time[4] > 59)return false;
		if(time[5] > 59)return false;
		
		return true;
	}
	/**
	 * 判断密码是否存在跨年的可能性(比如18年申请的两小时密码，19年才过期）
	 * 如果get方法输入的时间小于1月1日2点，则说明申请的时间是前一年
	 */
	public static boolean isTimeOverYear(final int [] time){
		if(time[1] == 1 && time[2] == 1 && time[3] < 2){
			return true;
		}
		return false;
	}
}



/////////////////////
