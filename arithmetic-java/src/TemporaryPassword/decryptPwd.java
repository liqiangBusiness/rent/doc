package TemporaryPassword;

import test.Print;
import test.testMain;

public class decryptPwd {
	/*
	 * 解密临时密码数据
	 * 返回值：int []
	 * index0-index5：开锁时间 （日时分） 
	 * index6-index8:hash值（用于计算该CRC值与锁内的密码+index1-6的时间是否一致）
	 * 
	 */
	public int [] getTimeAndHash(final int [] keyInputPwd,final int [] lockPwd) {
		int [] value = new int[9];
		int [] hash = new int[3];
		int [] num = new int[6];
		int [] time = new int[6];
		//解混淆
		int [] clear_pwd = tool.mixArrDe(keyInputPwd);
		
//		tool.pArr("keyInut ", keyInputPwd, " ");
//		tool.pArr("clear_pwd ", clear_pwd, " ");

		//time
		for(int i = 0;i<num.length;i++) {
			num[i] = clear_pwd[i];
		}
		//解time
		time = tool.arrSpicialSubtraction(num, lockPwd);
		for(int i=0;i<time.length;i++) {
			value[i] = time[i];
		}

		//赋值hash
		for(int i = 0;i<hash.length;i++) {
			value[6+i] = clear_pwd[num.length+i];
		}
		return value;
	}
	/*
	 * 获取时间   获取hash
	 * 是上面一个方法的两个具体化
	 */
	public int [] getTime(final int [] keyInputPwd,final int [] lockPwd){
		int [] data = getTimeAndHash(keyInputPwd,lockPwd);
		int [] time = new int[6];
		for(int i=0;i<time.length;i++) {
			time[i] = data[i];
		}
		return time;
	}
	public int [] getHash(final int [] keyInputPwd,final int [] lockPwd) {
		int [] data = getTimeAndHash(keyInputPwd,lockPwd);
		int [] hash = new int[3];
		for(int i=0;i<hash.length;i++) {
			hash[i] = data[i+6];
		}
		return hash;
	}
	/*
	 * 判断时间（日时分 0 4 1 2 4 5）是否合法
	 */
	public boolean checkTimeIsLegal(int [] time) {
		if(time.length>6)return false;
	    for(int i=0;i<6;i++){
	        if(time[i]<0)return false;
	    }
		if(time[0]>3)return false;
		if(time[1]>9)return false;
		if(time[2]>2) return false;
		if(time[3]>9)return false;
		if(time[4]>5)return false;
		if(time[5]>9)return false;
		return true;
	}
	/*
	 * 将时间数据日时分转化为分钟
	 */
	public int timeToMinutes(int [] time) {
		int num = 0;
		num += (time[0]*10+time[1] -1)*24*60;//日
		num += (time[2]*10+time[3])*60;//时
		num += (time[4]*10+time[5]);//分
		return num;
	}
	/*
	 * 校验数据
	 * 输入：final int [] realTime；锁内的时间的日时分,6位数组
	 * 返回值
	 * -1：时间不合法
	 * -2：数据校验错误
	 * -3：密码已经过期
	 * -4：密码正确时间合法，但是超出2小时范围
	 * 1：密码正确
	 */
	public int checkData(final int [] keyInputPwd,final int [] lockPwd,final int [] realTime) {
		int [] keyInputPwd_time = getTime(keyInputPwd,lockPwd);//日时分
		int [] keyInputPwdHash = getHash(keyInputPwd,lockPwd);
		tool.pArr("iput ", keyInputPwd_time, " ");
		tool.pArr("hash ", keyInputPwdHash, " ");

		
		//1判断time是否合法
		if(checkTimeIsLegal(keyInputPwd_time) == false)return -1;
		//check hash					lockPwd+keyInputPwd_time
		int [] InputTimeAndLockPwdHash = tool.getArrHashNum(keyInputPwd_time,lockPwd);
		if(Print.DEBUGE) {
			tool.pArr("lockPwd ",lockPwd," ");
			tool.pArr("deadtime ",keyInputPwd_time," ");	
			tool.pArr("hash ",InputTimeAndLockPwdHash," ");	
		}		
		
		
		for(int i=0;i<keyInputPwdHash.length;i++) {
			if(keyInputPwdHash[i] != InputTimeAndLockPwdHash[i])return -2;
		}
		//时间没有过期，判断是否超过2小时（120分钟）
		int t1 = timeToMinutes(keyInputPwd_time);
		int t2 = timeToMinutes(realTime);
		if(t1 - t2 < 0) {
			return -3;
		}
		if(t1 - t2 > 120) {
			return -4;
		}
		return 1;
	}
	int timeArr_to_int(int [] time){
		int num=0;
		num+=time[0]*100000;
		num+=time[1]*10000;
		num+=time[2]*1000;
		num+=time[3]*100;
		num+=time[4]*10;
		num+=time[5]*1;
		return num;
	}
	/*
	 * 校验数据
	 * 输入：final int [] realTime；锁内的时间的日时分,6位数组
	 * 返回值
	 * -1：时间不合法
	 * -2：数据校验错误
	 * -3：密码已经过期
	 * -4：密码正确时间合法，但是超出2小时范围
	 * 1：密码正确
	 */
	public int checkDataV2(final int [] keyInputPwd,final int [] lockPwd,final int [] realTime) {
		int [] keyInputPwd_time = getTime(keyInputPwd,lockPwd);//日时分
		int [] keyInputPwdHash = getHash(keyInputPwd,lockPwd);
		//check hash					lockPwd+keyInputPwd_time
		int [] InputTimeAndLockPwdHash = tool.getArrHashNum(keyInputPwd_time,lockPwd);

		if(Print.DEBUGE) {
			tool.pArr("锁内密码 ",lockPwd," ");
			tool.pArr("实时时间 ",realTime," ");	
			tool.pArr("失效时间 ",keyInputPwd_time," ");	
			tool.pArr("hash 校验",InputTimeAndLockPwdHash," ");	
			tool.pArr("hash 输入",keyInputPwdHash," ");
		}		
		
		for(int i=0;i<keyInputPwdHash.length;i++) {
			if(keyInputPwdHash[i] != InputTimeAndLockPwdHash[i])return -2;
		}
		//时间没有过期，判断是否超过2小时（120分钟）
		int t1 = timeArr_to_int(keyInputPwd_time);
		int t2 = timeArr_to_int(realTime);
		
		if(Print.DEBUGE) {
			System.out.println("t1 - t2 "+(t1-t2));
		}		
		
		if(t1 - t2 < 0) {
			return -3;
		}
		if(t1 - t2 > 120) {
			return -4;
		}
		System.out.println("时间差    "+(t1-t2));
		return 1;
	}
}
