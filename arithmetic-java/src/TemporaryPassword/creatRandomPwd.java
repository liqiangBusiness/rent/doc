package TemporaryPassword;

import test.Print;
import test.testMain;

public class creatRandomPwd {

	/*
	 * 创建一个临时密码
	 * 输入参数：
	 * 	int [] time：密码的过期时间，长度为6位数组，（日时分）
	 * 	int [] pwd：开锁密码，为超级密码或者时效密码，必须是6位
	 */
	public int [] get(int [] time,int [] pwd) {
		int [] arr = new int [9];
		//求和
		int [] arr1=tool.arrSpicialNum(time,pwd);//0-5
		//计算hash
		int [] arr2=tool.getArrHashNum(time,pwd);//6-8
		
//		if(Print.DEBUGE) {
//			tool.pArr("pwd ", pwd, " ");
//			tool.pArr("time ", time, " ");
//			tool.pArr("hash ", arr2, " ");
//		}	

		//赋值
		for(int i=0;i<arr1.length;i++) {
			arr[i] = arr1[i];
		}
		for(int i=0;i<arr2.length;i++) {
			arr[i+arr1.length] = arr2[i]; 
		}
		//混淆数据
		return tool.mixArrEn(arr);
	}
}
