package TemporaryPassword;
/*
 * 加密：
1.输入的第一个参数改为时间：time[] = {2018,12,12,12,12,12}数组
1.5 得到time的元时间：time0[]={2018,1,1,0,0,0};
2.计算分钟值：minute = time_to_mim(time) - time_to_mim(time0)
3.将minute转为6位数组 minute_arr[6]={}（不足6位高位补0）;传入原来的算法中



解密
1.读取锁内的时间：time_lock[]={2018,12,12,12,12,12};
1.5 得到time的元时间：time0[]={2018,1,1,0,0,0};
2.计算分钟值：minute = time_to_mim(time) - time_to_mim(time0)
3.将minute转为6位数组 minute_arr[6]={}（不足6位高位补0）;作为原算法的real_time
 */
public class randomPwdV2 extends creatRandomPwd{

	
	/*
	 * 创建一个临时密码
	 * 输入参数：
	 * 	@param int [] time：密码的过期时间，长度为6位数组，（年月日时分秒 {2018,12,12,12,12,12} ）
	 * 	@param int [] pwd：开锁密码，为超级密码或者时效密码，必须是6位
	 */
	@Override
	public int [] get(final int [] time,int [] pwd){
		//1.5
		int [] time0 = new int[6];
		if(tool.isTimeOverYear(time)){
			time0[0]=time[0] - 1;
			System.out.println("生成密码：该密码跨年----");
		}else{
			time0[0]=time[0];
		}
		time0[1]=1;
		time0[2]=1;
		time0[3]=0;
		time0[4]=0;
		time0[5]=0;
		//2
		int minutes = tool.time_to_minute(time) - tool.time_to_minute(time0);
		//3
		int [] time_minute = tool.intToArr(minutes,6);
		//4
		return super.get(time_minute, pwd);
	}
}
