package disvisionPwd;
//需求1：网关和服务器数据传输做字符串加密
/*
 * 加解密数字字符串
 * 加密字符串用在服务器上，解密方法用在锁上
 * 返回：混淆码
 * 异常情况：返回null
 */
public class DisvisionPwd {
	
static char en(char src) {
	char r = 0;
	switch(src) {
	case '0':r='A';break;
	case '1':r='C';break;
	case '2':r='E';break;
	case '3':r='G';break;
	case '4':r='H';break;
	case '5':r='M';break;
	case '6':r='R';break;
	case '7':r='S';break;
	case '8':r='Z';break;
	case '9':r='Q';break;
	default:r=src;break;
	}
	return r;
}
static char de(char src) {
	char r = 0;
	switch(src) {
	case 'A':r='0';break;
	case 'C':r='1';break;
	case 'E':r='2';break;
	case 'G':r='3';break;
	case 'H':r='4';break;
	case 'M':r='5';break;
	case 'R':r='6';break;
	case 'S':r='7';break;
	case 'Z':r='8';break;
	case 'Q':r='9';break;
	default:r=src;break;
	}
	return r;
}	
 public static String Encode(String src) {
		String returnValue = "";
		char temp=0;
		for(int i=0;i<src.length();i++) {
			temp = en((char)src.charAt(i));
			returnValue += temp;
		}
		return  returnValue;
	}
public static String Decode(String src) {
		String returnValue = "";
		char temp=0;
		for(int i=0;i<src.length();i++) {
			temp = de((char)src.charAt(i));
			returnValue += temp;
		}
		
		return  returnValue;
	}
}
