package test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import TemporaryPassword.creatRandomPwd;
import TemporaryPassword.decryptPwd;
import TemporaryPassword.tool;

public class testMainSerial {
	public static int arrToInt(int [] arr) {
		int sum=0;
		for(int i=0;i<arr.length;i++) {
			sum+=arr[i]*Math.pow(10,arr.length-1-i);
		}
		return sum;
	}
	public static int [] intToArr(int src) {
        int[] y = new int[String.valueOf(src).length()];
        for(int i=0;i<String.valueOf(src).length();i++)
        { 
            y[i]= String.valueOf(src).toCharArray()[i]-48;
        }
        return y;
	}
	public static void writeFile(String str,String filename){

			FileOutputStream o = null;
			byte[] buff = new byte[]{};
			try{
				File file = new File(filename);
			if(!file.exists()){
				file.createNewFile();
			}
			buff=str.getBytes();
			o=new FileOutputStream(file,true);
			o.write(buff);
			o.flush();
			o.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		//假设当前时间和锁内部的密码是固定的，枚举开锁密码0-999999999开锁成功次数
		int [] realTime = {0,4,1,2,3,4};		
		int [] lockRealPwd= {1,2,9,4,5,6};		
		int [] keyInputPwd = {0,0,0,0,0,0,0,0,0};
		int succesNum = 0;
		boolean b = true;
		String filename = "output.txt";
		decryptPwd decryptpwd = new decryptPwd();
		while(b) {
			int re = decryptpwd.checkData(keyInputPwd,lockRealPwd,realTime);//一次解密
			int buf = arrToInt(keyInputPwd);
			
			if(re == 1) {
				re=0;
				succesNum++;
//				tool.pArr("keyInputPwd ",keyInputPwd , " ");
				System.out.print(succesNum+"     ");
				String s = succesNum+"";
				writeFile(s+"     ",filename);
				System.out.println("-----"+buf);
				s = buf +"";
				writeFile("-----"+s+"\n",filename);
			}
			buf++;
			if(buf>999999999)b=false;
			keyInputPwd = intToArr(buf);
		}

		writeFile("over \n",filename);
	}
	

}
