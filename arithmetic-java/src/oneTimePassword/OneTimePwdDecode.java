package oneTimePassword;
import TemporaryPassword.*;
import test.Print;
public class OneTimePwdDecode extends decryptPwd{
	/*
	 * 校验数据
	 * 输入：final int [] realTime；锁内的时间的日时分,6位数组
	 * 返回值
	 * -1：时间不合法
	 * -2：数据校验错误
	 * -3：密码已经过期
	 * -4：密码正确时间合法，但是超出2小时范围
	 * 1：密码正确
	 */
	@Override
	public int checkData(final int [] keyInputPwd,final int [] lockPwd,final int [] realTime) {
		int checkDataResult = super.checkData(keyInputPwd, lockPwd, realTime);
		if(checkDataResult!=1){
			return checkDataResult;
		}
		//TODO 判断是否在锁里面使用过该密码  在C端实现
		return 1;
	}
}
