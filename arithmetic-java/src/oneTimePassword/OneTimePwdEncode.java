package oneTimePassword;
import TemporaryPassword.*;
public class OneTimePwdEncode extends creatRandomPwd{
	/*
	 * 混淆一个10位的数组
	 */
	final static int [] index10 = {1,2,8,6,5,9,4,0,7,3};//keep same as C
	public static int [] mixArr10En(int [] arr) {
		int [] out = new int[10];
		if(arr.length != 10) {
			out[0] = -1;
			return out;
		}
		for(int i=0;i<arr.length;i++) {
			out[i] = arr[index10[i]];
		}
		return out;
	}
	/*
	 * 解密混淆后的一个10位的数组
	 */
	public static int [] mixArr10De(int [] arr) {
		int [] out = new int[10];
		if(arr.length != 10) {
			out[0] = -1;
			return out;
		}
		for(int i=0;i<arr.length;i++) {
			out[index10[i]] = arr[i];
		}
		return out;
	}
	@Override
	public int [] get(int [] time,int [] pwd) {
		int [] arr = super.get(time, pwd);//前九位
		int [] value = new int[arr.length+1];
		for(int i=0;i<arr.length;i++) {
			value[i] = arr[i];
		}
		value[9]=1;
		
		tool.pArr("arr1   ",value, " ");
		return mixArr10En(value);
	}
}
