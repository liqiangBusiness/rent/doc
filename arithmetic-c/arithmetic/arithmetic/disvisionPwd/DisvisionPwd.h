//
//  DisvisionPwd.h
//  arithmetic
//
//  Created by liqiang on 2018/4/25.
//  Copyright © 2018 李强. All rights reserved.
//

#ifndef DisvisionPwd_h
#define DisvisionPwd_h

#include <stdio.h>
void disvisionEncode(const char * src,char *out);
void disvisionDecode(const char * src,char *out);
#endif /* DisvisionPwd_h */
