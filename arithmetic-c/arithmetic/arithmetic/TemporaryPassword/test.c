//
//  test.c
//  arithmetic
//
//  Created by liqiang on 2018/5/16.
//  Copyright © 2018 李强. All rights reserved.
//

#include "test.h"
#include "math.h"
#include<stdlib.h>

/*
 * 对6位数数组做+1处理
 * 如输入：{1，2，3}，输出{1，2，4}
 * 输入：{1，8，9}，输出{1，9，0}
 */
static int single_arr_to_int(const int *pwd,int len)
{
    u8 i;
    int sum=0;
    for(i=0;i<len;i++)
    {
        sum+=(pow(10,i)*pwd[len-1-i]);
    }
    return sum;
}
//函数自加
//param，number:自加的数值
void arraySelfAdd(const int  *src,int *Out,int number){
    int num = single_arr_to_int(src,6);
    num+=number;
    Out[0]=num/100000;
    Out[1]=(num%100000)/10000;
    Out[2]=(num%10000)/1000;
    Out[3]=(num%1000)/100;
    Out[4]=(num%100)/10;
    Out[5]=num%10;
}
/*
 * 数组复制
 */
void arrncpy(const int *src,int *out ,int len){
    int i=0;
    for(i=0;i<len;i++){
        out[i] = src[i];
    }
}
//数组归零
void memset_arr(int *arr,int target,int len){
    int i;
    for(i=0;i<len;i++){
        arr[i] = target;
    }
}
void pArr_(char *h ,const int * src,int len){
    int i;
    printf("%s  ",h);
    for(i=0;i<len;i++){
        printf("%d ",src[i]);
    }
}
void testTemporaryPwd(void){
    static long cycleTimes = 0,totalTimes = 0;
    int deadTime[6]={0};//失效时间
    int realTime[6]={0};//当前时间
    int unlockPwd[6]={0};//开锁密码
    int temporaryPwd[9]={0};//临时密码
    int result = 0;
    for(;;){
        if(checkTimeIsLegal(deadTime) == TRUE){
            getTemPwd(deadTime,unlockPwd,temporaryPwd);//加密
            arrncpy(deadTime, realTime, 6);//复制当前时间
            result = checkData(temporaryPwd,unlockPwd,realTime,deadTime);//解密
            
//            pArr_("time " ,deadTime,6);
//            pArr_("  pwd  " ,unlockPwd,6);
//            pArr_("  temp " ,temporaryPwd,9);
//            pArr_("  decode " ,&result,1);
            cycleTimes++;totalTimes++;
            if(result != 1){
                printf("ERROR!!!!!   \n");
                break;
            }else{
                //printf(" --->%10ld\n",cycleTimes);
            }
        }
        //时间下一位
        arraySelfAdd(deadTime,deadTime,1);
        //密码下一位
        if(single_arr_to_int(deadTime,6) == 999999){
            arraySelfAdd(unlockPwd,unlockPwd,rand()%100+1);//pwd 自加一个1--100的随机数 想要自加1则改为%1+1即可
            memset_arr(deadTime,0,6);
            memset_arr(realTime,0,6);
            
            //打印数据
            pArr_("  pwd  " ,unlockPwd,6);
            printf("   self add -->%10ld  /%10ld\n",cycleTimes,totalTimes);
            
            
            cycleTimes=0;

        }
        
        
        
    }
    
    
    
    
    
    
    
}
