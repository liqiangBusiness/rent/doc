//
//  decryptPwd.h
//  arithmetic
//
//  Created by liqiang on 2018/4/25.
//  Copyright © 2018 李强. All rights reserved.
//

#ifndef decryptPwd_h
#define decryptPwd_h

#include <stdio.h>
void getTimeAndHash(const  int * keyInputPwd,const int * lockPwd,int *value) ;
void getTime(const int * keyInputPwd,const int* lockPwd,int *returnTime);
void getHash(const int * keyInputPwd,const int * lockPwd,int *returnHash) ;
int checkData(const int* keyInputPwd,const int * lockPwd,const int * realTime,int * deadTimeOut);

extern int checkTimeIsLegal(int * time);
extern void pArr(char *h ,const int * src,int len);
#endif /* decryptPwd_h */
