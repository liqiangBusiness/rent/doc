//
//  test.h
//  arithmetic
//
//  Created by liqiang on 2018/5/16.
//  Copyright © 2018 李强. All rights reserved.
//

#ifndef test_h
#define test_h

#include <stdio.h>
#include "decryptPwd.h"
#include "passwordTool.h"
#include "encryptPwd.h"
typedef unsigned int u8;
#define FALSE 0
#define TRUE 1
void testTemporaryPwd(void);
#endif /* test_h */
