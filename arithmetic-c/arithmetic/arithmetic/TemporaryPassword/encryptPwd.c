//
//  encryptPwd.c
//  arithmetic
//
//  Created by liqiang on 2018/5/16.
//  Copyright © 2018 李强. All rights reserved.
//

#include "encryptPwd.h"
#include "passwordTool.h"

/*
 * 创建一个临时密码
 * 输入参数：
 *     int * time：密码的过期时间，长度为6位数组，（日时分）
 *     int * pwd：开锁密码，为超级密码或者时效密码，必须是6位
 */
void getTemPwd(const int * time,const int * pwd,int * tempPwdOut) {
 //   int [] arr = new int [9];
    int tempOutBuf[9]={0};
    int arr1[6]={0};
    int arr2[3]={0};
    int i=0;
    //求和
    arrSpicialNum(time,pwd,arr1);//0-5
    //计算hash
    getArrHashNum(time,pwd,arr2);//6-8
    
    
    //赋值
    for(i=0;i<6;i++) {
        tempOutBuf[i] = arr1[i];
    }
    for(i=0;i<3;i++) {
        tempOutBuf[i+6] = arr2[i];
    }
    //混淆数据
    mixArrEn(tempOutBuf,tempPwdOut);
    }
