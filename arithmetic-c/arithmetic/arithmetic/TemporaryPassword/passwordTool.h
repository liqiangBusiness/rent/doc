//
//  passwordTool.h
//  arithmetic
//
//  Created by liqiang on 2018/4/25.
//  Copyright © 2018 李强. All rights reserved.
//

#ifndef passwordTool_h
#define passwordTool_h

#include <stdio.h>

extern void arrSpicialNum(const int * time,const int * pwd,int *numReturnValue);
extern void arrSpicialSubtraction(const int * num,const int *pwd,int *returnTime);
extern void getArrHashNum(const int *time,const int *pwd,int crcNumArr[3]);
extern void mixArrDe(const int * arr,int *out);
extern void mixArrEn(int *arr,int *out);
extern int hashCode(const int * Source, const int len);
#endif /* passwordTool_h */
