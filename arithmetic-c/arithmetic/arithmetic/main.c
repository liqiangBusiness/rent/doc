//
//  main.c
//  arithmetic
//
//  Created by liqiang on 2018/4/25.
//  Copyright © 2018 李强. All rights reserved.
//

#include <stdio.h>
#include "decryptPwd.h"
#include "DisvisionPwd.h"
#include "string.h"
#include "encryptPwd.h"
#include "test.h"
#include "gateWayHashUtil.h"

void pArr(char *h ,const int * src,int len){
    int i;
    printf("%s  ",h);
    for(i=0;i<len;i++){
        printf("%d ",src[i]);
    }
    printf("\n");
}
void test1TemporaryPassword(void){
    int keyInputPwd[9]={7,8,0,3,1,4,9,1,1};
    int lockPwd[6]={1,2,3,4,5,6};
    int realTime[6]={0,5,1,4,3,4};
    int deadTimeOut[6]={0};
    int a = 0;
    a = checkData(keyInputPwd,lockPwd,realTime,deadTimeOut);
    printf("re is %d\n",a);
}
void test2DisvisionPwdDecode(void){
    int i;
    char *src="682|98|294409|20180328151641,684|42|761398|20180327173420";
    
    char out[300] = "";
    char out1[300]="";
    printf("原文  ");
    for(i=0;i<strlen(src);i++){
        printf("%c",src[i]);
    }printf("\n");
    disvisionEncode(src,out) ;
    printf("加密后 ");
    for(i=0;i<strlen(src);i++){
        printf("%c",out[i]);
    }printf("\n");
    
    disvisionDecode(out,out1) ;
    printf("解密后 ");
    for(i=0;i<strlen(src);i++){
        printf("%c",out1[i]);
    }printf("\n");

    
}
void test3CreatTemporaryPassword(){
    int lockPwd[6]={1,2,3,4,5,6};
    int realTime[6]={0,5,1,4,3,4};
    int n[9]={0};
    int i=0;
    getTemPwd(realTime,lockPwd,n);

    printf("临时密码  ");
    for(i=0;i<9;i++){
        printf("%d ",n[i]);
    }printf("\n");

    
}
void test4GateNoHash(void){
    char * gatewayNo = "10000008";
    char output[8]="";
    getGatewayNumberCode(gatewayNo,output);
    printf("gateway is %s  hash is %s\n",gatewayNo,output);
    
}
int main(int argc, const char * argv[]) {

    printf("Hello, World!\n");
//    test1TemporaryPassword();
//    test2DisvisionPwdDecode();
//    test3CreatTemporaryPassword();
//    testTemporaryPwd();
    test4GateNoHash();
    return 0;
}
