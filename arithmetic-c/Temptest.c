#include <stdio.h>
#include "math.h"
#include<stdlib.h>





/*
 * 解密临时密码数据
 * 返回值：int value[9]
 * index0-index5：开锁时间 （日时分）
 * index6-index8:hash值（用于计算该CRC值与锁内的密码+index1-6的时间是否一致）
 *
 */
void getTimeAndHash(const  int * keyInputPwd,const int * lockPwd,int *value) {
    int num[6] = {0};
    int time[6]={0};
    int clear_pwd[9] = {0};
    int i = 0;
    //解混淆
    
    mixArrDe(keyInputPwd,clear_pwd);

    //time
    for(i = 0;i<6;i++) {
        num[i] = clear_pwd[i];
    }
    //解time
    arrSpicialSubtraction(num,lockPwd,time);
    for(i=0;i<6;i++) {
        value[i] = time[i];
    }
    //赋值hash
    for(i = 0;i<3;i++) {
        value[6+i] = clear_pwd[6+i];
    }
    return;
}
/*
 * 获取时间   获取hash
 * 是上面一个方法的两个具体化
 */
void getTime(const int * keyInputPwd,const int* lockPwd,int *returnTime){
    int data[9]={0};
    int i=0;
    getTimeAndHash(keyInputPwd,lockPwd,data);
    for(i=0;i<6;i++) {
        returnTime[i] = data[i];
    }
    return ;
}
void getHash(const int * keyInputPwd,const int * lockPwd,int *returnHash) {
    int data[9]={0};
    int i=0;
    getTimeAndHash(keyInputPwd,lockPwd,data);
    for(i=0;i<3;i++) {
        returnHash[i] = data[i+6];
    }
    return ;
}
/*
 * 判断时间（日时分 0 4 1 2 4 5）是否合法
 */
int checkTimeIsLegal(int * time) {
	if(((time[0]*10+time[1])>=32) || ((time[0]*10+time[1]) == 0))//日
        return 0;
    if((time[2]*10+time[3]) >= 24)//时
        return 0;
    if((time[4]*10+time[5])>=60)//分
        return 0;
     return 1;
}
/*
 * 将时间数据日时分转化为分钟
 */
int timeToMinutes(const int * time) {
    int num = 0;
    num += (time[0]*10+time[1] -1)*24*60;//日
    num += (time[2]*10+time[3])*60;//时
    num += (time[4]*10+time[5]);//分
    return num;
}


/*
 * 校验数据
 * 输入：const int [] realTime；锁内的时间的日时分,6位数组
 * 输出：int * deadTimeOut ;该密码对应的失效时间
 * 返回值
 * -1：时间不合法
 * -2：数据校验错误
 * -3：密码已经过期
 * -4：密码正确时间合法，但是超出2小时范围
 * 1：密码正确
 */
int checkData(const int* keyInputPwd,const int * lockPwd,const int * realTime,int * deadTimeOut) {
    int keyInputPwd_time[6]= {0};
    int keyInputPwdHash[3]={0};
    int InputTimeAndLockPwdHash[3] = {0};
    int i = 0,t1=0,t2=0;
    getTime(keyInputPwd,lockPwd,keyInputPwd_time);//日时分
    getHash(keyInputPwd,lockPwd,keyInputPwdHash);
    for(i=0;i<6;i++){
        deadTimeOut[i] = keyInputPwd_time[i];
    }
    //1判断time是否合法
    if(checkTimeIsLegal(keyInputPwd_time) == 0){
        return -1;
    }
    //check hash                    lockPwd+keyInputPwd_time
    getArrHashNum(keyInputPwd_time,lockPwd,InputTimeAndLockPwdHash);
    
    for(i=0;i<3;i++) {
        if(keyInputPwdHash[i] != InputTimeAndLockPwdHash[i]){
            return -2;
        }
    }
    //时间有没有过期，判断是否超过2小时（120分钟）
    t1 = timeToMinutes(keyInputPwd_time);
    t2 = timeToMinutes(realTime);
    if( t1 - t2 < 0){
        return -3;
    }
    if(t1 - t2 > 120) {
        return -4;
    }
    return 1;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * 两个数组数据的特殊加法
 *     2 3 1 9 2 5
 *     8 9 7 6 4 5
 * +
 * ----------------
 *     0 2 8 5 8 0
 * 异常返回：-1：数据长度不合法；-2:数据内容不合法
 */
void arrSpicialNum(const int * time,const int * pwd,int *numReturnValue) {
//    int [] num = new int[6];
    int i=0;
    for(i=0;i<6;i++) {
        if(time[i]<0 || time[i]>9 || pwd[i]<0 || pwd[i] > 9) {
            numReturnValue[0] = -2;
            return;
        }
        numReturnValue[i] = time[i] + pwd[i];
        numReturnValue[i] = numReturnValue[i]>=10?numReturnValue[i]%10:numReturnValue[i];
    }
    return;
}

/*
 * 两个数组数据的特殊减法
 *     0 2 8 5 8 0
 *     8 9 7 6 4 5
 * -
 * ----------------
 *    2 3 1 9 2 5
 * 异常返回：-1：数据长度不合法；-2:数据内容不合法
 */
void arrSpicialSubtraction(const int * num,const int *pwd,int *returnTime) {
    int i=0;
    for(i=0;i<6;i++) {
        if(num[i]<0 || num[i]>9 || pwd[i]<0 || pwd[i] > 9) {
            returnTime[0] = -2;
            return;
        }
        returnTime[i] = num[i] - pwd[i];
        returnTime[i] = returnTime[i]<0?(returnTime[i]+10):returnTime[i];
    }
    return ;
}
/*
 * 获取两个数据叠加起来数据的CRC对应的数组
 */
void getArrHashNum(const int *time,const int *pwd,int crcNumArr[3]) {
    int num[12] = {0};
    int crcNum = 0;
    int i=0;
    for(i=0;i<6;i++) {
        num[i] = time[i];
        num[i+6] = pwd[i];
    }
    crcNum = hashCode(num, 12);
    crcNumArr[0] = crcNum/100;;
    crcNumArr[1] = crcNum/10%10;
    crcNumArr[2] = crcNum%10;
    return ;
}
/*
 * 混淆一个9位的数组
 */
const static int index[9] = {1,3,5,7,0,2,4,6,8};//keep same as java
void  mixArrEn(int *arr,int *out) {
    int i=0;
    for(i=0;i<9;i++) {
        out[i] = arr[index[i]];
    }
    return ;
}
/*
 * 解密混淆后的一个9位的数组
 */
void mixArrDe(const int * arr,int *out) {
    int i=0;
    for(i=0;i<9;i++) {
        out[index[i]] = arr[i];
    }
    return ;
}
/*
 * 获取数据的自定义hashCode
 * 返回：0--999
 */
int hashCode(const int * Source, const int len){
    const int  CRC8Table[] = {
        0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
        0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
        0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e,
        0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
        0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0,
        0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
        0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d,
        0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
        0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5,
        0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
        0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58,
        0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
        0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6,
        0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
        0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b,
        0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
        0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f,
        0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
        0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,
        0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
        0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c,
        0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
        0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1,
        0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
        0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49,
        0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
        0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4,
        0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
        0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a,
        0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
        0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7,
        0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35,
    };
    
    int sum=0,crc8=0;
    int i=0;
    for(i=0;i<len;i++){
        sum = sum ^ Source[i];
        if(sum<0)sum=0;
        if(sum>255)sum=255;
        crc8  = CRC8Table[sum];
    }
    return  crc8%1000;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
 * 创建一个临时密码
 * 输入参数：
 *     int * time：密码的过期时间，长度为6位数组，（日时分）
 *     int * pwd：开锁密码，为超级密码或者时效密码，必须是6位
 */
void getTemPwd(const int * time,const int * pwd,int * tempPwdOut) {
 //   int [] arr = new int [9];
    int tempOutBuf[9]={0};
    int arr1[6]={0};
    int arr2[3]={0};
    int i=0;
    //求和
    arrSpicialNum(time,pwd,arr1);//0-5
    //计算hash
    getArrHashNum(time,pwd,arr2);//6-8
    
    
    //赋值
    for(i=0;i<6;i++) {
        tempOutBuf[i] = arr1[i];
    }
    for(i=0;i<3;i++) {
        tempOutBuf[i+6] = arr2[i];
    }
    //混淆数据
    mixArrEn(tempOutBuf,tempPwdOut);
    }




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * 对6位数数组做+1处理
 * 如输入：{1，2，3}，输出{1，2，4}
 * 输入：{1，8，9}，输出{1，9，0}
 */
static int single_arr_to_int(const int *pwd,int len)
{
    u8 i;
    int sum=0;
    for(i=0;i<len;i++)
    {
        sum+=(pow(10,i)*pwd[len-1-i]);
    }
    return sum;
}
//函数自加
//param，number:自加的数值
void arraySelfAdd(const int  *src,int *Out,int number){
    int num = single_arr_to_int(src,6);
    num+=number;
    Out[0]=num/100000;
    Out[1]=(num%100000)/10000;
    Out[2]=(num%10000)/1000;
    Out[3]=(num%1000)/100;
    Out[4]=(num%100)/10;
    Out[5]=num%10;
}
/*
 * 数组复制
 */
void arrncpy(const int *src,int *out ,int len){
    int i=0;
    for(i=0;i<len;i++){
        out[i] = src[i];
    }
}
//数组归零
void memset_arr(int *arr,int target,int len){
    int i;
    for(i=0;i<len;i++){
        arr[i] = target;
    }
}
void pArr_(char *h ,const int * src,int len){
    int i;
    printf("%s  ",h);
    for(i=0;i<len;i++){
        printf("%d ",src[i]);
    }
}
void testTemporaryPwd(void){
    static long cycleTimes = 0,totalTimes = 0;
    int deadTime[6]={0};//失效时间
    int realTime[6]={0};//当前时间
    int unlockPwd[6]={0};//开锁密码
    int temporaryPwd[9]={0};//临时密码
    int result = 0;
    for(;;){
        if(checkTimeIsLegal(deadTime) == TRUE){
            getTemPwd(deadTime,unlockPwd,temporaryPwd);//加密
            arrncpy(deadTime, realTime, 6);//复制当前时间
            result = checkData(temporaryPwd,unlockPwd,realTime,deadTime);//解密
            
//            pArr_("time " ,deadTime,6);
//            pArr_("  pwd  " ,unlockPwd,6);
//            pArr_("  temp " ,temporaryPwd,9);
//            pArr_("  decode " ,&result,1);
            cycleTimes++;totalTimes++;
            if(result != 1){
                printf("ERROR!!!!!   \n");
                break;
            }else{
                //printf(" --->%10ld\n",cycleTimes);
            }
        }
        //时间下一位
        arraySelfAdd(deadTime,deadTime,1);
        //密码下一位
        if(single_arr_to_int(deadTime,6) == 999999){
            arraySelfAdd(unlockPwd,unlockPwd,rand()%100+1);//pwd 自加一个1--100的随机数 想要自加1则改为%1+1即可
            memset_arr(deadTime,0,6);
            memset_arr(realTime,0,6);
            
            //打印数据
            pArr_("  pwd  " ,unlockPwd,6);
            printf("   self add -->%10ld  /%10ld\n",cycleTimes,totalTimes);
            
            
            cycleTimes=0;

        }
    }
}

int main(int argc, const char * argv[]) {
    printf("Hello, World!\n");
    testTemporaryPwd();
    return 0;
}

